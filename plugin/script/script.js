(function($){           //myPlugin

    $.fn.myPlugin = function(obj){

        if(!obj){
            $(this).empty(); //without arguments remove child of element
        }
        else{
            if(obj === 'update'){
                $(this).empty();
                for(var i = 0;i<arguments[1].items.length;i++){  //update child of elements
                    $(this).append(arguments[1].items[i]);
                }
            }
            else{
                for(var i = 0;i<obj.items.length;i++){
                    $(this).append(obj.items[i]);       //add child of element
                }
            }
        }
    }

})(jQuery);

function getSelected(){             //create array with selected items
    var tags = [];
    if($('#div').is(':checked')){
        tags.push('div');
    }
    if($('#span').is(':checked')){
        tags.push('span');
    }
    if($('#a').is(':checked')){
        tags.push('a');
    }
    if($('#strong').is(':checked')){
        tags.push('strong');
    }
    if($('#i').is(':checked')){
        tags.push('i');
    }
    if($('#button').is(':checked')){
        tags.push('button');
    }
    return tags;
}

function createElements(data){
    var arrayOfElements = JSON.parse(data);     //get html markup from server
    var elements = {
        items : []
    };
    for(var i = 0;i<arrayOfElements.length;i++){
        elements.items.push($(arrayOfElements[i]).text("some text"));   //create DOM elements
    }
    return elements
}

function ajaxSuccessAdd(data){
    $('div').myPlugin(createElements(data));
}

function ajaxSuccessUpdate(data){
    $('div').myPlugin("update",createElements(data));
}


$('#remove-button').on('click',function(event){          //listening click events on the buttons
    event.preventDefault();
    $('div').myPlugin();
});

$('#add-button').on('click',function(){

    event.preventDefault();

        var selectedTags = getSelected();

        $.post(
            "http://plugin/data.php",
            {
                selected : selectedTags
            },
            ajaxSuccessAdd
        );

});

$('#update-button').on('click',function(){
    event.preventDefault();

    var selectedTags = getSelected();

    $.post(
        "http://plugin/data.php",
        {
            selected : selectedTags
        },
        ajaxSuccessUpdate
    );

});











