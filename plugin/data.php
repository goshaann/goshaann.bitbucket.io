<?php

    $result = [];

    $div = "<div></div>";
    $span = "<span></span>";
    $a = "<a></a>";
    $strong = "<strong></strong>";
    $i = "<i></i>";
    $button = "<button></button>";

    $fromClient = $_POST["selected"];

    if(in_array("div",$fromClient)){
        $result[] = $div;
    }
    if(in_array("span",$fromClient)){
        $result[] = $span;
    }
    if(in_array("a",$fromClient)){
        $result[] = $a;
    }
    if(in_array("strong",$fromClient)){
        $result[] = $strong;
    }
    if(in_array("i",$fromClient)){
        $result[] = $i;
    }
    if(in_array("button",$fromClient)){
        $result[] = $button;
    }

    echo(json_encode($result));

?>